var mongoose = require('mongoose');

var todoSchema = mongoose.Schema({
    done: Boolean,
    text: String,
    id: Number,
});


var Todo = mongoose.model('Todo', todoSchema);

module.exports = Todo;