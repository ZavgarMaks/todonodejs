
var url = 'mongodb://localhost:27017/todoNodejs';
var mongoose = require('mongoose');

mongoose.connect(url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('mongoose connection opened');
    // we're connected!
});


var Todo = require('./models/todos.model');

module.exports = {Todo: Todo}