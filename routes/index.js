var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var path = require('path');
var mongoose = require('mongoose');
var url = 'mongodb://localhost:27017/todoNodejs';

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile(path.resolve(__dirname+ '/../views/index.html'))
});
module.exports = router;
