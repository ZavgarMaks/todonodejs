
var Todo = require('../../db').Todo;

function index(req, res, next) {
    Todo.find(function (err, todos) {
        if (err){
            res.status(500).send(err);
            return console.error(err);
        }
        res.send(todos);
    })
}

function create(req,res,next) {
    var newTodo = new Todo(req.body);
    newTodo.save(function (err, todo) {
        console.log('err',todo);
        if (err){
            res.status(500).send(err);
            return console.error(err);
        }
        res.send(todo);
    });
}

function remove(req,res,next) {
    Todo.remove({id:req.params.id},function (err, todo) {
        console.log('err',todo);
        if (err){
            res.status(500).send(err);
            return console.error(err);
        }
        res.send(todo);
    });
}

function checkAll(req,res,next) {
    Todo.update({},{done:req.body.done},{multi:true},function(err, todos) {
        console.log('err',todos);
        if (err){
            res.status(500).send(err);
            return console.error(err);
        }
        res.send(todos);
    });
}

function editTodo(req,res,next) {
    Todo.update({id:req.params.id},{text:req.body.text, done:req.body.done},function(err, todo) {
        console.log('err',todo);
        if (err){
            res.status(500).send(err);
            return console.error(err);
        }
        res.send(todo);
    });
}

function removeCompleted(req,res,next) {
    Todo.remove({done:true},function(err, todos) {
        console.log('err',todos);
        if (err){
            res.status(500).send(err);
            return console.error(err);
        }
        res.send(todos);
    });
}
module.exports = {index: index,
    create: create,
    remove: remove,
    editTodo: editTodo,
    removeCompleted:removeCompleted,
    checkAll:checkAll};