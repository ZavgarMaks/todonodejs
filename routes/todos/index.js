var express = require('express');
var router = express.Router();

var controller = require('./todos.controller.js');



router.get('/',controller.index);
router.post('/', controller.create);
router.patch('/', controller.checkAll);
router.delete('/completed',controller.removeCompleted);
router.patch('/:id', controller.editTodo);
router.delete('/:id',controller.remove);


module.exports = router;
